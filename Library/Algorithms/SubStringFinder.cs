﻿using System;

namespace Library.Algorithms
{
    /// <summary>
    /// Поиск слова в тексте с использованием алгоритма Кнута-Морриса-Пратта.
    /// </summary>
    public static class SubStringFinder
    {
        /// <summary>
        /// Производит поиск подстроки substr в строке str. Возвращает индекс первого вхождения
        /// подстроки в строку или -1 если такая подстрока не найдена.
        /// </summary>
        public static int Find(string str, string substr)
        {
            int strLength = str.Length;
            int substrLength = substr.Length;

            if (strLength == 0 || substrLength == 0)
                return -1;

            int[] d = GetPrefixFunction(substr);

            int i;
            int j;
            for (i = 0, j = 0; (i < strLength) && (j < substrLength); i++, j++)
            {
                while ((j >= 0) && (substr[j] != str[i]))
                    j = d[j];
            }

            if (j == substrLength)
                return i - j;
            else
                return -1;
        }

        private static int[] GetPrefixFunction(string substr)
        {
            int length = substr.Length;
            int[] result = new int[length];

            int i = 0;
            int j = -1;
            result[0] = -1;
            while (i < length - 1)
            {
                while ((j >= 0) && (substr[j] != substr[i]))
                    j = result[j];

                i++;
                j++;

                if (substr[i] == substr[j])
                    result[i] = result[j];
                else
                    result[i] = j;
            }
            return result;
        }
    }
}
