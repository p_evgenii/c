﻿using System;
using System.Collections.Generic;

namespace Library.Collections.AvlTree
{
    public class AvlTree<TKey, TValue> : IEnumerable<AvlNode<TKey, TValue>> where TKey : IComparable<TKey>
    {
        private AvlNode<TKey, TValue> root;

        public AvlTree()
        {
            this.root = null;
        }
        /// <summary>
        /// Индексация дерева
        /// </summary>
        public TValue this[TKey key]
        {
            set
            {
                this.Insert(key, value);
            }
            get
            {
                TValue result;
                //Если ключ не найден, мы не можем изменить это значение.
                if (!this.TryGetValue(key, out result))
                {
                    throw new KeyNotFoundException();
                }
                return result;
            }
        }

        // Если узел с заданным ключом существует, то возвращаем True и выводим значение.
        // В противном случае возвращаем False и значение по умолчанию.

        /// <summary>
        /// Вывод значений из дерева
        /// </summary>
        public bool TryGetValue(TKey key, out TValue result)
        {
            AvlNode<TKey, TValue> current = root;
            while (current != null)
            {
                if (current.Key.CompareTo(key) == -1)
                {
                    current = current.RightChild;
                }
                else if (current.Key.CompareTo(key) == 1)
                {
                    current = current.LeftChild;
                }
                else
                {
                    result = current.Value;
                    return true;
                }
            }
            result = default(TValue);
            return false;
        }

        /// <summary>
        /// Проверка существования узла с заданным ключом
        /// </summary>
        public bool ContainsKey(TKey key)
        {
            TValue tmp;
            return this.TryGetValue(key, out tmp);
        }

        /// <summary>
        /// Вставка пары ключ-значение в дерево
        /// </summary>
        public void Insert(TKey key, TValue value)
        {
            //Если дерево пустое, то устанавливаем корень
            if (this.root == null)
            {
                this.root = new AvlNode<TKey, TValue>(key, value);
            }
            else
            {
                AvlNode<TKey, TValue> currentNode = root;
                while (currentNode != null)
                {
                    if (currentNode.Key.CompareTo(key) == -1)
                    {
                        if (currentNode.RightChild == null)
                        {
                            currentNode.RightChild = new AvlNode<TKey, TValue>(key, value, currentNode);
                            // Уменьшаем баланс узла на 1

                            InsertBalanceTree(currentNode, -1);
                            break;
                        }
                        else
                        {
                            currentNode = currentNode.RightChild;
                        }
                    }
                    else if (currentNode.Key.CompareTo(key) == 1)
                    {
                        if (currentNode.LeftChild == null)
                        {
                            currentNode.LeftChild = new AvlNode<TKey, TValue>(key, value, currentNode);
                            // Увеличиваем баланс узла на 1
                            InsertBalanceTree(currentNode, 1);
                            break;
                        }
                        else
                        {
                            currentNode = currentNode.LeftChild;
                        }
                    }
                    else
                    {
                        currentNode.Value = value;
                        break;
                    }
                }
            }
        }

        /// <summary>
        ///  Очистка дерева
        /// </summary>
        public void Clear()
        {
            this.root = null;
        }

        /// <summary>
        /// Регулировка баланса для узлов
        /// </summary>
        private void InsertBalanceTree(AvlNode<TKey, TValue> node, int addBalance)
        {
            while (node != null)
            {
                //Добавляем новое значение баланса в текущем узле.
                node.Balance += addBalance;

                // Если разница высот -1 или +1, то дерево сбалансированно
                if (node.Balance == 0)
                {
                    break;
                }
                // Если разница высот == 2
                else if (node.Balance == 2)
                {
                    if (node.LeftChild.Balance == 1)
                    {
                        SmallLeftRotation(node);
                    }
                    else
                    {
                        BigLeftRotation(node);
                    }
                    break;
                }

                // Если разница высот == -2
                else if (node.Balance == -2)
                {
                    if (node.RightChild.Balance == -1)
                    {
                        SmallRightRotation(node);
                    }
                    else
                    {
                        BigRightRotation(node);
                    }
                    break;
                }

                if (node.Parent != null)
                {
                    /*
                     * Если текущий узел является левым потомком родительского узла,
                     * мы должны увеличить высоту родительского узла.
                     * */
                    if (node.Parent.LeftChild == node)
                    {
                        addBalance = 1;
                    }
                    /*
                     * Если текущий узел является левым потомком родительского узла,
                     * мы должны уменьшать высоту родительского узла.
                     * */
                    else
                    {
                        addBalance = -1;
                    }
                }
                node = node.Parent;
            }
        }

        /// <summary>
        /// Малое правое вращение
        /// </summary>
        private void SmallRightRotation(AvlNode<TKey, TValue> node)
        {
            AvlNode<TKey, TValue> rightChild = node.RightChild;
            AvlNode<TKey, TValue> rightLeftChild = null;
            AvlNode<TKey, TValue> parent = node.Parent;

            if (rightChild != null)
            {
                rightLeftChild = rightChild.LeftChild;
                rightChild.Parent = parent;
                rightChild.LeftChild = node;
                rightChild.Balance++;
                node.Balance = -rightChild.Balance;
            }

            node.RightChild = rightLeftChild;
            node.Parent = rightChild;

            if (rightLeftChild != null)
            {
                rightLeftChild.Parent = node;
            }
            if (node == this.root)
            {
                this.root = rightChild;
            }
            else if (parent.RightChild == node)
            {
                parent.RightChild = rightChild;
            }
            else
            {
                parent.LeftChild = rightChild;
            }
        }

        /// <summary>
        /// Малое левое вращение
        /// </summary>
        private void SmallLeftRotation(AvlNode<TKey, TValue> node)
        {
            AvlNode<TKey, TValue> leftChild = node.LeftChild;
            AvlNode<TKey, TValue> leftRightChild = null;
            AvlNode<TKey, TValue> parent = node.Parent;

            if (leftChild != null)
            {
                leftRightChild = leftChild.RightChild;
                leftChild.Parent = parent;
                leftChild.RightChild = node;
                leftChild.Balance--;
                node.Balance = -leftChild.Balance;
            }

            node.Parent = leftChild;
            node.LeftChild = leftRightChild;

            if (leftRightChild != null)
            {
                leftRightChild.Parent = node;
            }

            if (node == this.root)
            {
                this.root = leftChild;
            }
            else if (parent.LeftChild == node)
            {
                parent.LeftChild = leftChild;
            }
            else
            {
                parent.RightChild = leftChild;
            }
        }

        /// <summary>
        /// Большое правое вращение
        /// </summary>
        private void BigRightRotation(AvlNode<TKey, TValue> node)
        {
            AvlNode<TKey, TValue> rightChild = node.RightChild;
            AvlNode<TKey, TValue> rightLeftChild = null;
            AvlNode<TKey, TValue> rightLeftRightChild = null;

            if (rightChild != null)
            {
                rightLeftChild = rightChild.LeftChild;
            }
            if (rightLeftChild != null)
            {
                rightLeftRightChild = rightLeftChild.RightChild;
            }

            node.RightChild = rightLeftChild;

            if (rightLeftChild != null)
            {
                rightLeftChild.Parent = node;
                rightLeftChild.RightChild = rightChild;
                rightLeftChild.Balance--;
            }

            if (rightChild != null)
            {
                rightChild.Parent = rightLeftChild;
                rightChild.LeftChild = rightLeftRightChild;
                rightChild.Balance--;
            }

            if (rightLeftRightChild != null)
            {
                rightLeftRightChild.Parent = rightChild;
            }

            SmallRightRotation(node);
        }

        /// <summary>
        /// Большое левое вращение
        /// </summary>
        private void BigLeftRotation(AvlNode<TKey, TValue> node)
        {
            AvlNode<TKey, TValue> leftChild = node.LeftChild;
            AvlNode<TKey, TValue> leftRightChild = leftChild.RightChild;
            AvlNode<TKey, TValue> leftRightLeftChild = null;
            if (leftRightChild != null)
            {
                leftRightLeftChild = leftRightChild.LeftChild;
            }

            node.LeftChild = leftRightChild;

            if (leftRightChild != null)
            {
                leftRightChild.Parent = node;
                leftRightChild.LeftChild = leftChild;
                leftRightChild.Balance++;
            }

            if (leftChild != null)
            {
                leftChild.Parent = leftRightChild;
                leftChild.RightChild = leftRightLeftChild;
                leftChild.Balance++;
            }

            if (leftRightLeftChild != null)
            {
                leftRightLeftChild.Parent = leftChild;
            }


            SmallLeftRotation(node);
        }

        /// <summary>
        /// Удаление узла по заданному ключу
        /// </summary>
        public void Delete(TKey key)
        {
            AvlNode<TKey, TValue> current = this.root;
            while (current != null)
            {
                if (current.Key.CompareTo(key) == -1)
                {
                    current = current.RightChild;
                }
                else if (current.Key.CompareTo(key) == 1)
                {
                    current = current.LeftChild;
                }
                else //Поиск ключа
                {
                    if (current.LeftChild == null && current.RightChild == null)
                    {
                        if (current == root)
                        {
                            root = null;
                        }
                        else if (current.Parent.RightChild == current)
                        {
                            current.Parent.RightChild = null;
                            DeleteBalanceTree(current.Parent, 1);
                        }
                        else
                        {
                            current.Parent.LeftChild = null;
                            DeleteBalanceTree(current.Parent, -1);
                        }
                    }
                    else if (current.LeftChild != null) //Получить наименьшее узел из левого поддерева.
                    {
                        AvlNode<TKey, TValue> rightMost = current.LeftChild;
                        while (rightMost.RightChild != null)
                        {
                            rightMost = rightMost.RightChild;
                        }


                        ReplaceNodes(current, rightMost);
                        DeleteBalanceTree(rightMost.Parent, 1);
                    }
                    else //Получить наименьшее узел из правого поддерева.
                    {
                        AvlNode<TKey, TValue> leftMost = current.RightChild;
                        while (leftMost.LeftChild != null)
                        {
                            leftMost = leftMost.LeftChild;
                        }

                        ReplaceNodes(current, leftMost);
                        DeleteBalanceTree(leftMost.Parent, -1);
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Замена двух узлов и регулировка детей и родительских связей.
        /// </summary>
        private void ReplaceNodes(AvlNode<TKey, TValue> sourceNode, AvlNode<TKey, TValue> subtreeNode)
        {
            sourceNode.Key = subtreeNode.Key;
            sourceNode.Value = subtreeNode.Value;

            if (subtreeNode.Parent != null)
            {
                if (subtreeNode.LeftChild != null)
                {
                    subtreeNode.LeftChild.Parent = subtreeNode.Parent;
                    if (subtreeNode.Parent.LeftChild == subtreeNode)
                    {
                        subtreeNode.Parent.LeftChild = subtreeNode.LeftChild;
                    }
                    else
                    {
                        subtreeNode.Parent.RightChild = subtreeNode.LeftChild;
                    }
                }
                else if (subtreeNode.RightChild != null)
                {
                    subtreeNode.RightChild.Parent = subtreeNode.Parent;
                    if (subtreeNode.Parent.LeftChild == subtreeNode)
                    {
                        subtreeNode.Parent.LeftChild = subtreeNode.RightChild;
                    }
                    else
                    {
                        subtreeNode.Parent.RightChild = subtreeNode.RightChild;
                    }
                }
                else
                {
                    if (subtreeNode.Parent.LeftChild == subtreeNode)
                    {
                        subtreeNode.Parent.LeftChild = null;
                    }
                    else
                    {
                        subtreeNode.Parent.RightChild = null;
                    }
                }
            }
        }

        /// <summary>
        /// Регулировка баланса после удаления узла
        /// </summary>
        private void DeleteBalanceTree(AvlNode<TKey, TValue> node, int addBalance)
        {
            while (node != null)
            {
                node.Balance += addBalance;
                addBalance = node.Balance;

                if (node.Balance == 2)
                {
                    if (node.LeftChild != null && node.LeftChild.Balance >= 0)
                    {
                        SmallLeftRotation(node);

                        if (node.Balance == -1)
                        {
                            return;
                        }
                    }
                    else
                    {
                        BigLeftRotation(node);
                    }
                }
                else if (node.Balance == -2)
                {
                    if (node.RightChild != null && node.RightChild.Balance <= 0)
                    {
                        SmallRightRotation(node);

                        if (node.Balance == 1)
                        {
                            return;
                        }
                    }
                    else
                    {
                        BigRightRotation(node);
                    }
                }
                else if (node.Balance != 0)
                {
                    return;
                }

                AvlNode<TKey, TValue> parent = node.Parent;

                if (parent != null)
                {
                    if (parent.LeftChild == node)
                    {
                        addBalance = -1;
                    }
                    else
                    {
                        addBalance = 1;
                    }
                }
                node = parent;
            }
        }

        /// <summary>
        /// Прямой обход дерева
        /// </summary>
        public IEnumerator<AvlNode<TKey, TValue>> GetEnumerator()
        {
            Queue<AvlNode<TKey, TValue>> queue = new Queue<AvlNode<TKey, TValue>>();
            queue.Enqueue(this.root);

            AvlNode<TKey, TValue> tmp;
            while (queue.Count > 0)
            {
                tmp = queue.Dequeue();

                if (tmp.LeftChild != null)
                {
                    queue.Enqueue(tmp.LeftChild);
                }
                if (tmp.RightChild != null)
                {
                    queue.Enqueue(tmp.RightChild);
                }

                yield return tmp;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}