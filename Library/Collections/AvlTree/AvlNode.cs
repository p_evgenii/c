﻿using System;

namespace Library.Collections.AvlTree
{
    public class AvlNode<TKey, TValue>
    {   
        public TKey Key { get; set; }
        public TValue Value { get; set; }
        public AvlNode<TKey, TValue> Parent { get; set; }
        public AvlNode<TKey, TValue> LeftChild { get; set; }
        public AvlNode<TKey, TValue> RightChild { get; set; }
        public int Balance { get; set; }

        public AvlNode(TKey key, TValue value)
        {
            this.Key = key;
            this.Value = value;
            this.LeftChild = null;
            this.RightChild = null;
            this.Parent = null;
            this.Balance = 0;
        }

        public AvlNode(TKey key, TValue value, AvlNode<TKey, TValue> parent)
            : this(key, value)
        {
            this.Parent = parent;
        }

        public AvlNode(AvlNode<TKey, TValue> node)
            : this(node.Key, node.Value, node.Parent)
        {
            this.LeftChild = node.LeftChild;
            this.RightChild = node.RightChild;
        }
    }
}