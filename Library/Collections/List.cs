﻿using System;

namespace Library.Collections
{
    public class List<T>
    {
        private class Node<TItem>
        {
            public TItem Item { get; set; }
            public Node<TItem> Next { get; set; }
        }

        private Node<T> _head;

        private Node<T> LastNode
        {
            get
            {
                var temp = _head;

                while (temp.Next != null)
                    temp = temp.Next;

                return temp;
            }
        }

        /// <summary>
        /// Количество элементов в списке.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Добавляет элемент item в конец списка.
        /// </summary>
        public void Add(T item)
        {
            var node = new Node<T>() { Item = item };

            if (_head == null)
                _head = node;
            else
                LastNode.Next = node;

            Count++;
        }

        /// <summary>
        /// Удаляет первый найденный элемент item.
        /// </summary>
        public void Remove(T item)
        {
            if (_head == null)
                return;

            if (_head.Item.Equals(item))
            {
                _head = _head.Next;
                Count--;
            }
            else
            {
                Node<T> beforeCurrent = null;
                Node<T> current = _head;

                while (current.Next != null)
                {
                    if (current.Item.Equals(item))
                    {
                        beforeCurrent.Next = current.Next;
                        Count--;
                        break;
                    }

                    beforeCurrent = current;
                    current = current.Next;
                }
            }
        }

        /// <summary>
        /// Преобразует список в массив.
        /// </summary>
        public T[] ToArray()
        {
            var result = new T[Count];

            if (_head != null)
            {
                var temp = _head;

                for (int i = 0; i < Count; i++, temp = temp.Next)
                    result[i] = temp.Item;
            }

            return result;
        }

        /// <summary>
        /// Возвращает true если найден хотя бы один элемент удовлетворяющий заданному условию.
        /// Записывает в result первый найденный элемент.
        /// </summary>
        public bool Find(Func<T, bool> findCondition, out T result)
        {
            Node<T> current = _head;

            while (current.Next != null)
            {
                if (findCondition(current.Item))
                {
                    result = current.Item;
                    return true;
                }

                current = current.Next;
            }

            result = default(T);
            return false;
        }

        /// <summary>
        /// Сортирует список с применением указанной в параметре comparer функции сравнения.
        /// </summary>
        public void Sort(Func<T, T, bool> comparer)
        {
            _head = MergeSort(_head, comparer);
        }

        #region Sort implementation

        private Node<T> MergeSort(Node<T> head, Func<T, T, bool> comparer)
        {
            if (head == null || head.Next == null)
                return head;

            Node<T> middle = GetListMiddle(head);
            Node<T> headOfSecondHalf = middle.Next;
            middle.Next = null;

            return Merge(MergeSort(head, comparer), MergeSort(headOfSecondHalf, comparer), comparer);
        }

        private Node<T> Merge(Node<T> a, Node<T> b, Func<T, T, bool> comparer)
        {
            Node<T> nodeBeforeHead = new Node<T>();
            Node<T> current = nodeBeforeHead;

            while (a != null && b != null)
            {
                if (comparer(a.Item, b.Item))
                {
                    current.Next = a;
                    a = a.Next;
                }
                else
                {
                    current.Next = b;
                    b = b.Next;
                }

                current = current.Next;
            }

            current.Next = (a == null) ? b : a;
            return nodeBeforeHead.Next;
        }

        private Node<T> GetListMiddle(Node<T> head)
        {
            if (head == null)
                return head;

            Node<T> slow = head;
            Node<T> fast = head;

            while (fast.Next != null && fast.Next.Next != null)
            {
                slow = slow.Next;
                fast = fast.Next.Next;
            }

            return slow;
        }

        #endregion
    }
}
