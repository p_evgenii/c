﻿using System;

namespace Library.Collections
{
    class KeyValuePair<TValue>
    {
        public string Key { get; private set; }
        public TValue Value { get; private set; }

        public KeyValuePair (string key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }

    public class HashTable<TValue>
    {
        private KeyValuePair<TValue>[] _data; // Хэш таблица
        private bool[] _delete; // Пометка о удаленном элементе 
        private int _maxSize; // Максимальное количество элементов

        public int Count { get; private set; } // Количество записей

        /// <summary>
        /// Инициализация хэш таблицы
        /// </summary>
        /// <param name="maxSize">Размер</param>
        public HashTable (int maxSize)
        {
            _maxSize = maxSize;
            _data = new KeyValuePair<TValue>[_maxSize];
            _delete = new bool[_maxSize];
        }

        /// <summary>
        /// Добавление элемента в хеш таблицу
        /// </summary>
        public bool Add (string key, TValue item) // 
        {
            if (key == null || Contains(key))
                return false;

            int index = 0;
            for (int i = 0; i < _maxSize; i++)
            {
                index = GetHashIndex(key, i);
                if (_data[index] == null)
                {
                    _data[index] = new KeyValuePair<TValue>(key, item);
                    _delete[index] = false;
                    Count++;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Удаление элемента из хеш-таблицы
        /// </summary>
        public bool Remove (string key)
        {
            int index = 0;
            for (int i = 0; i < _maxSize; i++)
            {
                index = GetHashIndex(key, i);
                if (_data[index] != null)
                {
                    if (_data[index].Key == key)
                    {
                        _data[index] = null;
                        _delete[index] = true;
                        Count--;
                        return true;
                    }
                }
            }
            return false;
        }


        /// <summary>
        /// Проверка вхождения ключа в хеш-таблицу
        /// </summary>
        public bool Contains (string key)
        {
            if (key != null)
            {
                int index = 0;

                for (int i = 0; i < _maxSize; i++)
                {
                    index = GetHashIndex(key, i);
                    if (_data[index] != null && _data[index].Key == key && !_delete[index])
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Поиск записи в хеш-таблице по ключу
        /// </summary>
        public bool Find (string key, out TValue value)
        {
            if (key != null)
            {
                int index = 0;

                for (int i = 0; i < _maxSize; i++)
                {
                    index = GetHashIndex(key, i);
                    if (_data[index] != null && _data[index].Key == key && !_delete[index])
                    {
                        value = _data[index].Value;
                        return true;
                    }
                }
            }

            value = default(TValue);
            return false;
        }

        /// <summary>
        /// Очистка хэш-таблицы
        /// </summary>
        public void Clear ()
        {
            for (int i = 0; i < _maxSize; i++)
            {
                _data[i] = null;
                _delete[i] = false;
            }

            Count = 0;
        }

        private int GetHashIndex (string key, int i) // Индекс
        {
            return Math.Abs((GetMainHashCode(key) + i * GetSecondHashCode(key)) % _maxSize);
        }

        private int GetMainHashCode (string key) // Основная хэш функция
        {
            int hash = 0;

            foreach (char ch in key)
                hash = 33 * hash + ch;
            return hash;
        }

        private int GetSecondHashCode (string key) // Второстепенная хэш функция
        {
            int hash = 0;

            foreach (char ch in key)
                hash = 29 * hash + ch;
            return hash;
        }
    }
}