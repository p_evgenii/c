﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.UI
{
    public class ConsoleMenu
    {
        private string _title;
        private List<string> _descriptions = new List<string>();
        private List<Action> _actions = new List<Action>();
        private bool _readMenuIndex = true;

        public ConsoleMenu(string title)
        {
            _title = title;
        }

        public void AddMenuItem(string description, Action action)
        {
            _descriptions.Add(description);
            _actions.Add(action);
        }

        public void Show()
        {
            AddMenuItem("Выход.", Close);

            while (_readMenuIndex)
            {
                Console.Clear();
                Console.WriteLine(_title + "\n");

                for (int i = 0; i < _descriptions.Count; i++)
                    Console.WriteLine("{0, 2}. {1}", i + 1, _descriptions[i]);

                Console.Write("\nВыбранный пункт меню: ");

                string answer = Console.ReadLine();
                int menuIndex;

                if (int.TryParse(answer, out menuIndex) && CheckMenuIndex(menuIndex))
                {
                    Console.Clear();
                    _actions[menuIndex - 1]();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Введён неверный номер! Нажмите любую клавишу и повторите попытку.");
                    Console.ReadKey();
                }
            }
        }

        private bool CheckMenuIndex(int index)
        {
            if (index > 0 && index <= _descriptions.Count)
                return true;
            else
                return false;
        }

        private void Close()
        {
            _readMenuIndex = false;
        }
    }
}
