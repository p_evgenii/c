﻿using Library.Data;
using Library.Collections;
using Library.Collections.AvlTree;
using System;
using Library.UI;

namespace Library
{
    class Program
    {
        static void Main(string[] args)
        {
            int Max = 127; // Максимальное количество читателей
            HashTable<Person> HashTablePerson = new HashTable<Person>(Max); // Хэш таблица

            AvlTree<string, Book> AVLTreeBook = new AvlTree<string, Book>(); // АВЛ дерево

            Collections.List<BookOperation> ListBookOperations = new Collections.List<BookOperation>(); // Список

            var menu = new ConsoleMenu("Выберите пункт меню:");

            menu.AddMenuItem("Регистрация нового читателя", ExampleAction);
            menu.AddMenuItem("Снятие читателя с обслуживания", ExampleAction);
            menu.AddMenuItem("Просмотр всех зарегистрированных читателей", ExampleAction);
            menu.AddMenuItem("Очистка данных о читателях", ExampleAction);
            menu.AddMenuItem("Поиск читателя по номеру читательского билета", ExampleAction);
            menu.AddMenuItem("Поиск читателя по ФИО", ExampleAction);
            menu.AddMenuItem("Добавление новой книги", ExampleAction);
            menu.AddMenuItem("Удаление сведений о книге", ExampleAction);
            menu.AddMenuItem("Просмотр всех имеющихся книг", ExampleAction);
            menu.AddMenuItem("Очистка данных о книгах", ExampleAction);
            menu.AddMenuItem("Поиск книги по шифру", ExampleAction);
            menu.AddMenuItem("Поиск книги по ФИО автора", ExampleAction);
            menu.AddMenuItem("Выдача книги", ExampleAction);
            menu.AddMenuItem("Возврат книги", ExampleAction);

            menu.Show();
        }

        static void ExampleAction()
        {
            Console.WriteLine("Функционал не написан!");
            Console.ReadKey();
        }
    }
}