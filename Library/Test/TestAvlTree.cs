﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library.Collections;
using Library.Data;
using Library.AvlTree;


namespace Library.Test
{
    [TestClass]
    public class TestAvlTree
    {
        Book book_1 = new Book { Id = "001.001", Author = "Уильям Шекспир", Title = "Гамлет", Publisher = "", PublishingYear = 1963, TotalCopies = 10, AvailableCopies = 3 };
        Book book_2 = new Book { Id = "002.001", Author = "Лев Николаевич Толстой", Title = "Война и мир", Publisher = "", PublishingYear = 1869, TotalCopies = 10, AvailableCopies = 3 };
        Book book_3 = new Book { Id = "003.001", Author = "Ден Браун", Title = "Цифровая крепость", Publisher = "", PublishingYear = 1998, TotalCopies = 10, AvailableCopies = 3 };
        Book book_4 = new Book { Id = "001.002", Author = "Уильям Шекспир", Title = "Ромэо и Джульетта", Publisher = "", PublishingYear = 1597, TotalCopies = 10, AvailableCopies = 3 };
        Book book_5 = new Book { Id = "004.001", Author = "Мигель де Сервантес", Title = "Дон Кихот", Publisher = "", PublishingYear = 1605, TotalCopies = 10, AvailableCopies = 3 };

        [TestMethod]
        public void GetExistValue()
        {
            Book getBook;
            AvlTree<string, Book> tree = new AvlTree<string, Book>();
            tree.Insert("001.001", book_1);
            tree.TryGetValue("001.001", out getBook);
            Assert.AreEqual(book_1, getBook);
        }
    }
}
