﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Library.Collections;

namespace Library.Test
{
    [TestClass]
    public class TestList
    {
        private class ComplexType
        {
            public string name;
            public int number;
        }

        [TestMethod]
        public void Add()
        {
            var list = InitIntList();
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { 8, 7, 12, 10, 3, 1, -5 });

            list.Add(10);
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { 8, 7, 12, 10, 3, 1, -5, 10 });
        }

        [TestMethod]
        public void Remove()
        {
            var list = InitIntList();
            list.Remove(7);

            Assert.AreEqual(list.Count, 6);
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { 8, 12, 10, 3, 1, -5 });
        }

        [TestMethod]
        public void RemoveHead()
        {
            var list = InitIntList();
            list.Remove(8);

            Assert.AreEqual(list.Count, 6);
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { 7, 12, 10, 3, 1, -5 });
        }

        [TestMethod]
        public void RemoveInEmptyList()
        {
            var list = new List<int>();
            list.Remove(8);
            Assert.AreEqual(list.Count, 0);
        }

        [TestMethod]
        public void RemoveNotContainedItem()
        {
            var list = InitIntList();
            list.Remove(100);

            Assert.AreEqual(list.Count, 7);
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { 8, 7, 12, 10, 3, 1, -5 });
        }

        [TestMethod]
        public void Count()
        {
            var list = new List<int>();
            Assert.AreEqual(list.Count, 0);
            list.Add(5);
            Assert.AreEqual(list.Count, 1);
            list.Add(5);
            list.Add(5);
            Assert.AreEqual(list.Count, 3);
            list.Remove(5);
            Assert.AreEqual(list.Count, 2);
            list.Remove(5);
            list.Remove(5);
            Assert.AreEqual(list.Count, 0);
        }

        [TestMethod]
        public void FindSimpleType()
        {
            var list = InitIntList();
            int item;
            bool find = list.Find(a => a == 8, out item);
            Assert.IsTrue(find);
            Assert.AreEqual(item, 8);
        }

        [TestMethod]
        public void FindComplexType()
        {
            var list = InitComplexList();
            ComplexType item;
            bool find = list.Find(a => a.name == "seven", out item);
            Assert.IsTrue(find);
            Assert.AreEqual(item.number, 7);
        }

        [TestMethod]
        public void FindNotContainedItem()
        {
            var list = InitComplexList();
            ComplexType item;
            bool find = list.Find(a => a.name == "not contained", out item);
            Assert.IsFalse(find);
            Assert.AreEqual(item, null);
        }

        [TestMethod]
        public void SortAscending()
        {
            var list = InitIntList();
            list.Sort((a, b) => a < b);
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { -5, 1, 3, 7, 8, 10, 12 });
        }

        [TestMethod]
        public void SortDescending()
        {
            var list = InitIntList();
            list.Sort((a, b) => a > b);
            AssertArrayElementsAreEqual(list.ToArray(), new int[] { 12, 10, 8, 7, 3, 1, -5 });
        }

        private void AssertArrayElementsAreEqual(int[] actual, int[] expected)
        {
            for (int i = 0; i < actual.Length; i++)
                Assert.AreEqual(actual[i], expected[i]);
        }

        private List<int> InitIntList()
        {
            var list = new List<int>();
            list.Add(8);
            list.Add(7);
            list.Add(12);
            list.Add(10);
            list.Add(3);
            list.Add(1);
            list.Add(-5);
            return list;
        }

        private List<ComplexType> InitComplexList()
        {
            var list = new List<ComplexType>();
            list.Add(new ComplexType { name = "three", number = 3 });
            list.Add(new ComplexType { name = "zero", number = 0 });
            list.Add(new ComplexType { name = "one", number = 1 });
            list.Add(new ComplexType { name = "six", number = 6 });
            list.Add(new ComplexType { name = "two", number = 2 });
            list.Add(new ComplexType { name = "seven", number = 7 });
            list.Add(new ComplexType { name = "five", number = 5 });
            list.Add(new ComplexType { name = "four", number = 4 });
            return list;
        }
    }
}
