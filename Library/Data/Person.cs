﻿namespace Library.Data
{
    public class Person
    {
        public string Id { get; set; } // Номер читательского билета
        public string Name { get; set; } // ФИО читателя
        public string WorkPlace { get; set; } // Место работы/учебы читателя
        public string Address { get; set; } // Адрес читателя
        public int Birthday { get; set; } // Дата рождения читателя
    }
}
