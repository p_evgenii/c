﻿namespace Library.Data
{
    public class BookOperation
    {
        public string PersonId { get; set; } // Читательский билет
        public string BookId { get; set; } // Шифр книги
        public string IsUsedDate { get; set; } // Дата выдачи книги
        public string ReturnDate { get; set; } // Дата возврата книги
    }
}
