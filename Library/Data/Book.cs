﻿namespace Library.Data
{
    public class Book
    {
        public string Id { get; set; } // Шифр книги
        public string Author { get; set; } // Авторы книги
        public string Title { get; set; } // Название книги
        public string Publisher { get; set; } // Издательство книги
        public int PublishingYear { get; set; } // Год издания книги
        public int TotalCopies { get; set; } // Всего копий книг
        public int AvailableCopies { get; set; } // Копий книг в наличии
    }
}
