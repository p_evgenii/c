﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library.Collections.AvlTree;
using Library.Data;
using System.Collections.Generic;


namespace Library
{
    [TestClass]
    public class TestAvlTree
    {

        Book book_1 = new Book { Id = "001.001", Author = "Уильям Шекспир", Title = "Гамлет", Publisher = "", PublishingYear = 1963, TotalCopies = 10, AvailableCopies = 3 };
        Book book_2 = new Book { Id = "002.001", Author = "Лев Николаевич Толстой", Title = "Война и мир", Publisher = "", PublishingYear = 1869, TotalCopies = 10, AvailableCopies = 3 };
        Book book_3 = new Book { Id = "003.001", Author = "Ден Браун", Title = "Цифровая крепость", Publisher = "", PublishingYear = 1998, TotalCopies = 10, AvailableCopies = 3 };
        Book book_4 = new Book { Id = "001.002", Author = "Уильям Шекспир", Title = "Ромэо и Джульетта", Publisher = "", PublishingYear = 1597, TotalCopies = 10, AvailableCopies = 3 };
        Book book_5 = new Book { Id = "004.001", Author = "Мигель де Сервантес", Title = "Дон Кихот", Publisher = "", PublishingYear = 1605, TotalCopies = 10, AvailableCopies = 3 };

        [TestMethod]
        public void GetExistValue()
        {
            Book getBook;
            AvlTree<string, Book> tree = new AvlTree<string, Book>();
            tree.Insert("001.001", book_1);
            tree.TryGetValue("001.001", out getBook);
            Assert.AreEqual(book_1, getBook);
        }

        [TestMethod]
        public void Enumerator()
        {
            var tree = InitTree();
            var keys = new List<string>();
            var values = new List<Book>();

            foreach (var item in tree)
            {
                keys.Add(item.Key);
                values.Add(item.Value);
            }

            Assert.AreEqual(keys.Count, 5);
            Assert.AreEqual(values.Count, 5);

            Assert.IsTrue(keys.Contains(book_1.Id));
            Assert.IsTrue(keys.Contains(book_2.Id));
            Assert.IsTrue(keys.Contains(book_3.Id));
            Assert.IsTrue(keys.Contains(book_4.Id));
            Assert.IsTrue(keys.Contains(book_5.Id));

            Assert.IsTrue(values.Contains(book_1));
            Assert.IsTrue(values.Contains(book_2));
            Assert.IsTrue(values.Contains(book_3));
            Assert.IsTrue(values.Contains(book_4));
            Assert.IsTrue(values.Contains(book_5));
        }

        private AvlTree<string, Book> InitTree()
        {
            var tree = new AvlTree<string, Book>();
            tree.Insert("001.001", book_1);
            tree.Insert("002.001", book_2);
            tree.Insert("003.001", book_3);
            tree.Insert("001.002", book_4);
            tree.Insert("004.001", book_5);

            return tree;
        }


    }
}
