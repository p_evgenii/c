﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Library.Collections;

namespace Library
{
    [TestClass]
    public class TestHashTable
    {
        [TestMethod]
        public void TestSearchValueExist() // Объект присутствует
        {
            HashTable<int> ht = new HashTable<int>(127);
            ht.Add("first", 10);
            
            int expected = 10;
            int actual;
            Assert.IsTrue(ht.Find("first", out actual));
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSearchValueUnExist() // Объект отсутствует
        {
            HashTable<int> ht = new HashTable<int>(127);
            ht.Add("first", 10);

            int expected = 10;
            int actual;
            Assert.IsFalse(ht.Find("second", out actual));
            Assert.AreNotEqual(expected, actual);
        }

        [TestMethod]
        public void TestSearchNull() // Поиск нулевого объекта
        {
            HashTable<int> ht = new HashTable<int>(127);
            ht.Add("first", 10);

            int expected = 10;
            int actual;
            Assert.IsFalse(ht.Find(null, out actual));
            Assert.AreNotEqual(expected, actual);
        }

        [TestMethod]
        public void TestAddToFullHashTable () // Переполнение
        {
            HashTable<int> ht = new HashTable<int>(1);
            ht.Add("zero", 10);

            Assert.IsFalse(ht.Add("first", 10));
        }

        [TestMethod]
        public void TestAddValue() // Добавление объекта
        {
            HashTable<int> ht = new HashTable<int>(127);
            Assert.IsTrue(ht.Add("first", 10));
        }

        [TestMethod]
        public void TestAddNullKey() // Добавление нулевого ключа
        {
            HashTable<int> ht = new HashTable<int>(127);
            Assert.IsFalse(ht.Add(null, 10));
        }

        [TestMethod]
        public void TestRemoveObjectExist () // Удаление присутствующего элемента
        {
            HashTable<int> ht = new HashTable<int>(127);
            ht.Add("first", 10);

            Assert.IsTrue(ht.Remove("first"));
            int value;
            Assert.IsFalse(ht.Find("first", out value));
            Assert.AreEqual(ht.Count, 0);
        }

        [TestMethod]
        public void TestRemoveValueUnExist() // Удаление отсутствующего элемента
        {
            HashTable<int> ht = new HashTable<int>(127);
            ht.Add("first", 10);

            Assert.IsFalse(ht.Remove("second"));
            Assert.AreEqual(ht.Count, 1);
        }

        [TestMethod]
        public void TestAddExistKey() // Коллизия
        {
            HashTable<int> ht = new HashTable<int>(127);
            ht.Add("first", 10);

            Assert.IsFalse(ht.Add("first", 110));
            Assert.AreEqual(ht.Count, 1);
        }
    }
}
