﻿using Library.Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Library
{
    [TestClass]
    public class TestSubStringFinder
    {
        [TestMethod]
        public void FindExistSubString()
        {
            var strings = new List<string> { "Apple", "Orange", "Banana" };
            var subStrings = new List<string> { "ppl", "Or", "nana" };
            var expectResults = new int[] { 1, 0, 2 };

            for (int i = 0; i < strings.Count; i++)
            {
                Assert.AreEqual(SubStringFinder.Find(strings[i], subStrings[i]), expectResults[i]);
            }
        }

        [TestMethod]
        public void FindUnexistSubString()
        {
            var strings = new List<string> { "Apple", "Orange", "Banana" };
            var subStrings = new List<string> { "ass", "gen", "nanas" };

            for (int i = 0; i < strings.Count; i++)
            {
                Assert.AreEqual(SubStringFinder.Find(strings[i], subStrings[i]), -1);
            }
        }

        [TestMethod]
        public void FindNullInString()
        {
             Assert.AreEqual(SubStringFinder.Find("", "aaa"), -1);
        }

        [TestMethod]
        public void FindNullSubstring()
        {
            Assert.AreEqual(SubStringFinder.Find("sss", ""), -1);
        }
    }
}
